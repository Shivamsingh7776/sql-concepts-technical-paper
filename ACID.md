# ACID Properties in DBMS
In order to maintain the consistency in a database, before and after the transaction, certain properties are followed. These are called ACID properties.

## A = Atomicity:
It means if any operation is performed on the data, either it should be performed or executed completely or should not be executed at all.
1. Example :- Bank account 

## C = Consistency:
The word consistency means that the value should be remain preserved always.In other word the database must be consistent before and after the transaction.
1. Example :- In an application that transfers funds from one account to another, the consistency property ensures that the total value of funds in both the accounts is the same at the start and end of each transaction.

## I = Isolation:
The term 'Isolation' means separation.In DBMS,Isolation is the property of a database where no data should affect the other one and may occur concurrently.In short, Multiple transaction occur independently without interference.
1. Example :- In an application that transfers funds from one account to another, the isolation property ensures that another transaction sees the transferred funds in one account or the other, but not in both, nor in neither.

## D = Durability:
Durability ensures that the permanency of something.In DBMS, The term durability ensures that the data after the successful execution of the operation becomes permanent in the database.In short, The changes of a successful transaction occurs even if the system failure occurs.
1. Example :- When in an application system failure occurs after that we can easily access the old data from database because durability make them permanent in database.
