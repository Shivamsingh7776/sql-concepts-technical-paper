
# Normal Forms in DBMS:
Normalization is the process of minimizing redundancy from a relation or set of relations. Redundancy in relation may cause insertion, deletion, and update anomalies. So, it helps to minimize the redundancy in relations. Normal forms are used to eliminate or reduce redundancy in database table.

## 1NF(First Normal Form):
If a relation contain composite or multi-valued attribute, it violates first normal form or a relation is in first normal form if it does not contain any composite or multi-valued attribute. A relation is in first normal form if every attribute in that relation is singled valued attribute.
1. Example:- Employee Id of any company

## 2NF(Second Normal Form):
A relation will be in 2NF if it is in 1NF and all non-key attributes are fully functional dependent on the primary key.Or in other word To be in second normal form, a relation must be in first normal form and relation must not contain any partial dependency. A relation is in 2NF if it has No Partial Dependency, i.e., no non-prime attribute (attributes which are not part of any candidate key) is dependent on any proper subset of any candidate key of the table.
### Partial Dependency :- 
If the proper subset of candidate key determines non-prime attribute, it is called partial dependency.
1. Example:- Employee name cannot decide the employee id we need more information about the employee

## 3NF(Third Normal Form):
For a relation to be in Third Normal Form, it must be in Second Normal form .A relation is in third normal form, if there is no transitive dependency for non-prime attributes as well as it is in second normal form.A relation is in 3NF if at least one of the following condition holds in every non-trivial function dependency X –> Y

1. X is a super key.
2. Y is a prime attribute (each element of Y is part of some candidate key).
### Transitive dependency :
If A->B and B->C are two FDs then A->C is called transitive dependency.

## Boyce-Codd Normal Form (BCNF):
Boyce-Codd Normal Form (BCNF) is an extension of Third Normal Form on strict terms. BCNF states that
1. For any non-trivial functional dependency, X → A, X must be a super-key.

2. Example:-For example consider relation R(A, B, C)
A -> BC,
B ->
A and B both are super keys so above relation is in BCNF.

## 4NF(Four Normal Form):
A relation will be in 4NF if it is in Boyce Codd's normal form and has no multi-valued dependency.
## 5NF(Five Normal Form):
A relation is in 5NF. If it is in 4NF and does not contain any join dependency, joining should be lossless.

