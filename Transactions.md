
# SQL | TRANSACTIONS
## What are Transactions?
Transactions group a set of tasks into a single execution unit. Each transaction begins with a specific task and ends when all the tasks in the group successfully complete. If any of the tasks fail, the transaction fails. Therefore, a transaction has only two results: success or failure. 
Example of a transaction to transfer $150 from account A to account B:
1. read(A)
2.  A := A – 150
3. write(A)
4. read(B)
5.  B := B + 150
6. write(B)

Incomplete steps result in the failure of the transaction. A database transaction, by definition, must be atomic, consistent, isolated and durable. 
These are popularly known as ACID properties.  These properties can ensure the concurrent execution of multiple transactions without conflict.\

## How to implement Transactions using SQL?
Following commands are used to control transactions. It is important to note that these statements cannot be used while creating tables and are only used with the DML Commands such as – INSERT, UPDATE and DELETE

### BEGIN TRANSACTION: 
It indicates the start point of an explicit or local transaction. 
#### Syntax:
```sql
BEGIN TRANSACTION transaction_name ;
```
### SET TRANSACTION:
Places a name on a transaction. 
#### Syntax:
```sql
SET TRANSACTION [ READ WRITE | READ ONLY ];
```
### COMMIT:
If everything is in order with all statements within a single transaction, all changes are recorded together in the database is called committed. The COMMIT command saves all the transactions to the database since the last COMMIT or ROLLBACK command. 
#### Syntax: 
```sql
COMMIT;
```
#### Queries: 
It will delete the row with age 20.
```sql
DELETE FROM Student WHERE AGE = 20;
COMMIT;
```
### ROLLBACK: 
If any error occurs with any of the SQL grouped statements, all changes need to be aborted. The process of reversing changes is called rollback. This command can only be used to undo transactions since the last COMMIT or ROLLBACK command was issued. 
#### Syntax: 
```sql
ROLLBACK;
```
From the above example Sample table1, 
Delete those records from the table which have age = 20 and then ROLLBACK the changes in the database. 
#### Queries: 
```sql
DELETE FROM Student WHERE AGE = 20;
ROLLBACK;
```
It will return the deleted value into the table.
### SAVEPOINT: 
creates points within the groups of transactions in which to ROLLBACK. 
A SAVEPOINT is a point in a transaction in which you can roll the transaction back to a certain point without rolling back the entire transaction. 
#### Syntax 
```sql
SAVEPOINT SAVEPOINT_NAME;
```
This command is used only in the creation of SAVEPOINT among all the transactions. 
In general ROLLBACK is used to undo a group of transactions. 
#### Syntax for rolling back to Savepoint command: 
```sql
ROLLBACK TO SAVEPOINT_NAME;
```
You can ROLLBACK to any SAVEPOINT at any time to return the appropriate data to its original state. 
#### Queries: 
```sql
SAVEPOINT SP1;
--Savepoint created.
DELETE FROM Student WHERE AGE = 20;
--deleted
SAVEPOINT SP2;
--Savepoint created.
```
From the above example Sample table1, 
Delete those records from the table which have age = 20 and then ROLLBACK the changes in the database by keeping Savepoints. 
###  RELEASE SAVEPOINT:
 This command is used to remove a SAVEPOINT that you have created. 
#### Syntax: 
```sql
RELEASE SAVEPOINT SAVEPOINT_NAME
```
Once a SAVEPOINT has been released, you can no longer use the ROLLBACK command to undo transactions performed since the last SAVEPOINT.
It is used to initiate a database transaction and used to specify characteristics of the transaction that follows.
